import scrapy
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

class BrickSetSpider(scrapy.Spider):
    name = 'brick_spider'
    start_urls = ['http://www.hamropatro.com/news']

    def parse(self, response):
        SET_SELECTOR = '.item'
        for brickset in response.css(SET_SELECTOR):

            NEWS_NAME = 'h2 a ::text'
            SOURCE = 'div.source ::text'
            IMAGE = 'img ::attr(src)'
            DESCRIPTION = 'div.desc ::text'
            READ_MORE = 'div.readon a'

            print '\n' + brickset.css(NEWS_NAME).extract_first() #gives news heading
            print brickset.css(SOURCE).extract_first() #gives news source
            print brickset.css(DESCRIPTION).extract_first() #news demo
            print brickset.css(IMAGE).extract_first() #news image
            print brickset.css(READ_MORE).extract_first() #read more link